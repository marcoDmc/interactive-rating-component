const box = document.querySelector('.box')
const boxState = document.querySelector('.box__state')
const btn = document.querySelector('.box__button-submit')
const optionsOne = document.querySelector('#one')
const optionsTwo = document.querySelector('#two')
const optionsThree = document.querySelector('#three')
const optionsFour = document.querySelector('#four')
const optionsFive = document.querySelector('#five')
const ratingSupport = document.querySelector('.box__state-selected')

class Submit {
  handleSubmitButton() {
    if (ratingSupport.innerText === '') return
    box.classList.toggle('box__hide')
    boxState.classList.toggle('box__state-active')
  }
  handleOptions() {
    optionsOne.addEventListener('click', () => {
      optionsOne.style.backgroundColor = 'hsl(0, 0%, 100%)'
      optionsTwo.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsThree.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsFour.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsFive.style.backgroundColor = 'hsl(213, 19%, 18%)'
      ratingSupport.innerText = `   you selected ${optionsOne.innerText} out of 5 `
    })
    optionsTwo.addEventListener('click', () => {
      optionsTwo.style.backgroundColor = 'hsl(0, 0%, 100%)'
      optionsOne.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsThree.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsFour.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsFive.style.backgroundColor = 'none'
      ratingSupport.innerText = `   you selected ${optionsTwo.innerText} out of 5 `
    })
    optionsThree.addEventListener('click', () => {
      optionsThree.style.backgroundColor = 'hsl(0, 0%, 100%)'
      optionsTwo.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsOne.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsFour.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsFive.style.backgroundColor = 'hsl(213, 19%, 18%)'
      ratingSupport.innerText = `   you selected ${optionsThree.innerText} out of 5 `
    })
    optionsFour.addEventListener('click', () => {
      optionsFour.style.backgroundColor = 'hsl(0, 0%, 100%)'
      optionsTwo.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsThree.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsOne.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsFive.style.backgroundColor = 'hsl(213, 19%, 18%)'
      ratingSupport.innerText = `   you selected ${optionsFour.innerText} out of 5 `
    })
    optionsFive.addEventListener('click', () => {
      optionsFive.style.backgroundColor = 'hsl(0, 0%, 100%)'
      optionsTwo.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsThree.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsFour.style.backgroundColor = 'hsl(213, 19%, 18%)'
      optionsOne.style.backgroundColor = 'hsl(213, 19%, 18%)'
      ratingSupport.innerText = `   you selected ${optionsFive.innerText} out of 5 `
    })
  }
}
const submit = new Submit()

btn.addEventListener('click', submit.handleSubmitButton)
submit.handleOptions()
